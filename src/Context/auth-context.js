import React, { useState, useEffect } from "react";

const AuthContext = React.createContext({
  isLoggedIn: false,
  onLogout: () => {},
  onLogin: (email, password) => {},
});

export const AuthContextProvider = (props) => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    const storageIsLogin = localStorage.getItem("isLogin");
    if (storageIsLogin === "1") {
      setIsLoggedIn(true);
    }
  }, []);

  const handlerLogout = () => {
    localStorage.removeItem("isLogin");
    setIsLoggedIn(false);
  };

  const handlerLogin = () => {
    localStorage.setItem("isLogin", "1");
    setIsLoggedIn(true);
  };

  return (
    <AuthContext.Provider
      value={{
        isLoggedIn: isLoggedIn,
        onLogout: handlerLogout,
        onLogin: handlerLogin,
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthContext;
